package com.example.firstservice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employee")
public class FirstController {
	
	@GetMapping("message")
	public ResponseEntity<?> test() {
		return ResponseEntity.ok("Hello from first service");
	}
}
