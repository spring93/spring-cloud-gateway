Sumber :
- https://www.javainuse.com/spring/cloud-gateway

Documentation :
- https://spring.io/projects/spring-cloud

Tools :
- maven
- spring boot
- java 1.8
- eclipse IDE

jalankan gateway, firs-service & second-service :
- mvn spring-boot:run

Test first service :
- curl --location --request GET 'http://127.0.0.1:8083/employee/message'

Test second service :
- curl --location --request GET 'http://127.0.0.1:8084/consumer/message'

Test first service via gateway :
- curl --location --request GET 'http://127.0.0.1:8082/employee/message'

Test second service via gateway
- curl --location --request GET 'http://127.0.0.1:8082/consumer/message'