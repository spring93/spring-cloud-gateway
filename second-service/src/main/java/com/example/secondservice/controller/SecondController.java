package com.example.secondservice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("consumer")
public class SecondController {
	
	@GetMapping("message")
	public ResponseEntity<?> test() {
		return ResponseEntity.ok("Hello from second service");
	}
}